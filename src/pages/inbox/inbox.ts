import { Profile } from './../../model/profile.interface';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Message } from './../../model/message.interface';

/**
 * Generated class for the InboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {

  msgList: Observable<any>;
  myuid: string;
  myName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase, private af: AngularFireAuth) {
    this.myuid = this.af.auth.currentUser.uid;
    this.db.object(`/profiles/${this.myuid}`).valueChanges().subscribe(
      (p:Profile) => { this.myName = p.firstName+' '+p.lastName;}
     );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InboxPage');

    console.log(`/last-message/${this.myuid}`);

    this.msgList 
      = this.db.list(`/last-message/${this.myuid}`)
          .valueChanges().pipe(map( changes => {
            changes.map( (lastmsg: {msgkey: string, msg: string, peerName: string, peerUID: string, unread: number})=> {
              this.db.object(`/messages/${lastmsg.msgkey}`).valueChanges().subscribe(
                 (x:Message)=> { if(x) {
                  console.log(lastmsg.msgkey);
                  console.log(x);
                   lastmsg.msg = x.msg;
                   lastmsg.peerUID = x.fromID!=this.myuid ? x.fromID : x.toID;
                   this.db.object(`/profiles/${lastmsg.peerUID}`).valueChanges().subscribe(
                    (p:Profile) => { lastmsg.peerName = p.firstName+' '+p.lastName;}
                   );
                 }
                 }
              );
            });
            return changes;
          }));
  }
  
  onMsgClick(msg) {
    this.navCtrl.push('MessagePage', 
    { peeruid: msg.peerUID,
      peerName: msg.peerName, 
      myName: this.myName} );
  }
}
